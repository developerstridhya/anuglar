import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./auth/guards/auth.guards";

const routes: Routes = [
  {
    path: "authentication",
    loadChildren: () =>
      import("./pages/authentication/authentication.module").then(
        (m) => m.AuthenticationModule
      ),
  },
  {
    path: "dashboard",
    loadChildren: () =>
      import("./layout/layout.module").then((m) => m.LayoutModule),
    canActivate: [AuthGuard],
  },
  {
    path: "miscellaneous",
    loadChildren: () =>
      import("./pages/miscellaneous/miscellaneous.module").then(
        (m) => m.MiscellaneousModule
      ),
  },
  {
    path: "",
    redirectTo: "/dashboard/analytics",
    pathMatch: "full",
  },
  {
    path: "**",
    redirectTo: "/miscellaneous/error", //Error 404 - Page not found
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: "enabled",
      relativeLinkResolution: "legacy",
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
