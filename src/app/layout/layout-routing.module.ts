import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LayoutComponent } from "./layout.component";

const routes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      {
        path: "analytics",
        loadChildren: () =>
          import("../pages/dashboard/analytics/analytics.module").then(
            (m) => m.AnalyticsModule
          ),
      },
      {
        path: "account-settings",
        loadChildren: () =>
          import(
            "../pages/dashboard/account-settings/account-settings.module"
          ).then((m) => m.AccountSettingsModule),
      },
      {
        path: "e-commerce",
        loadChildren: () =>
          import("../pages/dashboard/ecommerce/ecommerce.module").then(
            (m) => m.EcommerceModule
          ),
      },
      {
        path: "profile",
        loadChildren: () =>
          import("../pages/dashboard/profile/profile.module").then(
            (m) => m.ProfileModule
          ),
      },
      {
        path: "user",
        loadChildren: () =>
          import("../pages/dashboard/user/user.module").then(
            (m) => m.UserModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayoutRoutingModule {}
