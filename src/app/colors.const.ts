export const colors = {
  solid: {
    primary: "#05537c",
    secondary: "#82868b",
    success: "#28C76F",
    info: "#00cfe8",
    warning: "#05537c",
    danger: "#EA5455",
    dark: "#4b4b4b",
    black: "#000",
    white: "#fff",
    body: "#f8f8f8",
  },
  light: {
    primary: "#05537c1a",
    secondary: "#82868b1a",
    success: "#28C76F1a",
    info: "#00cfe81a",
    warning: "#05537c1a",
    danger: "#EA54551a",
    dark: "#4b4b4b1a",
  },
};
