import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ComingSoonComponent } from "./coming-soon/coming-soon.component";
import { ErrorComponent } from "./error/error.component";
import { MaintenanceComponent } from "./maintenance/maintenance.component";
import { NotAuthorizedComponent } from "./not-authorized/not-authorized.component";

// Miscellaneous page Routes
const routes: Routes = [
  {
    path: "coming-soon",
    component: ComingSoonComponent,
  },
  {
    path: "not-authorized",
    component: NotAuthorizedComponent,
  },
  {
    path: "maintenance",
    component: MaintenanceComponent,
  },
  {
    path: "error",
    component: ErrorComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MiscellaneousRoutingModule {}
