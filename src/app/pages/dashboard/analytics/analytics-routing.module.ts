import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { Role } from "app/models/role";
import { DashboardService } from "app/services/dashboard.service";
import { AnalyticsComponent } from "./analytics.component";

// Analytics Routes
const routes: Routes = [
  {
    path: "",
    component: AnalyticsComponent,
    data: { roles: [Role.Admin], animation: "danalytics" },
    resolve: {
      css: DashboardService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnalyticsRoutingModule {}
