import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { EcommerceEditService } from "app/services/ecommerce-edit.service";
import { EcommerceService } from "app/services/ecommerce.service";
import { EcommerceDetailsComponent } from "./ecommerce-details/ecommerce-details.component";
import { EcommerceEditComponent } from "./ecommerce-edit/ecommerce-edit.component";
import { EcommerceShopComponent } from "./ecommerce-shop/ecommerce-shop.component";

// Ecommerce Page Routes
const routes: Routes = [
  {
    path: "shop",
    component: EcommerceShopComponent,
    resolve: {
      ecommerce: EcommerceService,
    },
    data: { animation: "EcommerceShopComponent" },
  },
  {
    path: "details/:id",
    component: EcommerceDetailsComponent,
    resolve: {
      ecommerce: EcommerceService,
    },
    data: { animation: "EcommerceDetailsComponent" },
  },
  {
    path: "details",
    redirectTo: "/dashboard/e-commerce/details/27", //Redirection
    data: { animation: "EcommerceDetailsComponent" },
  },
  {
    path: "edit/:id",
    component: EcommerceEditComponent,
    resolve: {
      ecommerce: EcommerceEditService,
    },
    data: { animation: "EcommerceEditComponent" },
  },
  {
    path: "edit",
    redirectTo: "/dashboard/e-commerce/details/27", //Redirection
    data: { animation: "EcommerceDetailsComponent" },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EcommerceRoutingModule {}
