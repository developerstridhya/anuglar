import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { NouisliderModule } from "ng2-nouislider";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import {
  SwiperConfigInterface,
  SwiperModule,
  SWIPER_CONFIG,
} from "ngx-swiper-wrapper";

import { UpsertProductSidebarComponent } from "./upsert-product-sidebar/upsert-product-sidebar.component";
import { EcommerceEditComponent } from "./ecommerce-edit/ecommerce-edit.component";

import { SharedModule } from "app/shared/shared.module";
import { EcommerceShopComponent } from "./ecommerce-shop/ecommerce-shop.component";
import { EcommerceDetailsComponent } from "./ecommerce-details/ecommerce-details.component";
import { EcommerceEditService } from "app/services/ecommerce-edit.service";
import { EcommerceSidebarComponent } from "./ecommerce-shop/sidebar/sidebar.component";
import { EcommerceItemComponent } from "./ecommerce-item/ecommerce-item.component";
import { EcommerceRoutingModule } from "./ecommerce-routing.module";

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: "horizontal",
  observer: true,
};

@NgModule({
  declarations: [
    EcommerceShopComponent,
    EcommerceSidebarComponent,
    EcommerceDetailsComponent,
    EcommerceItemComponent,
    EcommerceEditComponent,
    UpsertProductSidebarComponent,
  ],
  imports: [
    CommonModule,
    EcommerceRoutingModule,
    SwiperModule,
    FormsModule,
    NgbModule,
    NouisliderModule,
    SharedModule,
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG,
    },
  ],
})
export class EcommerceModule {}
