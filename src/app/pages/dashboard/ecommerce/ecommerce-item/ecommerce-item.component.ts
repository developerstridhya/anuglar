import { Component, Input, OnInit, ViewEncapsulation } from "@angular/core";

import { EcommerceService } from "app/services/ecommerce.service";

import { Subscription } from "rxjs";

@Component({
  selector: "app-ecommerce-item",
  templateUrl: "./ecommerce-item.component.html",
  styleUrls: ["./ecommerce-item.component.scss"],
  encapsulation: ViewEncapsulation.None,
  host: { class: "ecommerce-application" },
})
export class EcommerceItemComponent implements OnInit {
  // Input Decorotor
  @Input() product;

  // public
  public loading: any;
  get isMatching() {
    return this.productId == this.product?.id;
  }

  // private
  private productId: any;
  private loaderSub: Subscription;

  /**
   *
   * @param {EcommerceService} _ecommerceService
   */
  constructor(private _ecommerceService: EcommerceService) {}

  // Public Methods
  // -----------------------------------------------------------------------------------------------------

  // Toggle product availability
  toggleProductAvailibility(productId) {
    this.productId = productId;
    // Apply change to product loader stream
    this._ecommerceService.onProductEditLoader.next(true);
    const tempProduct = {
      ...this.product,
      isDisabled: !this.product.isDisabled,
    };
    // Apply product edit to stream
    this._ecommerceService.onProductEditChange.next(tempProduct);
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  ngOnInit(): void {
    this.loaderSub = this._ecommerceService.onProductEditLoader.subscribe(
      (res) => {
        this.loading = res;
      }
    );
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this.loaderSub?.unsubscribe();
  }
}
