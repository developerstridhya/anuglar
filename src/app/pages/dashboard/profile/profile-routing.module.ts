import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "app/auth/guards/auth.guards";
import { ProfileService } from "app/services/profile.service";
import { ProfileComponent } from "./profile.component";

// Profile page Routes
const routes: Routes = [
  {
    path: "",
    component: ProfileComponent,
    canActivate: [AuthGuard],
    resolve: {
      profile: ProfileService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
