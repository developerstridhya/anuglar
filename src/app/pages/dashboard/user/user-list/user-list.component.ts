import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { ColumnMode, DatatableComponent } from "@swimlane/ngx-datatable";

import { Subject } from "rxjs";
import { debounceTime, takeUntil, tap } from "rxjs/operators";

import { FormControl } from "@angular/forms";
import { Page } from "app/models/user";
import { CoreSidebarService } from "app/services/core-sidebar.service";
import { CoreConfigService } from "app/services/config.service";
import { UserListService } from "app/services/user-list.service";

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class UserListComponent implements OnInit {
  // Public
  public sidebarToggleRef = false;
  public rows;
  public page = new Page();
  public ColumnMode = ColumnMode;
  public loading = false;
  public selectedPlan = [];
  public selectedRole;
  public selectedStatus;
  public searchValue = "";
  public searchControl = new FormControl("");
  public error = "";

  public selectStatus: any = [
    { name: "All", value: "" },
    { name: "Active", value: "1" },
    { name: "Inactive", value: "2" },
  ];
  public selectRole: any = [
    { name: "All", value: "" },
    { name: "Admin", value: "1" },
    { name: "Client", value: "2" },
  ];

  // private
  private allUserParams: any;

  // Decorator
  @ViewChild(DatatableComponent) table: DatatableComponent;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   * @param {UserListService} _userListService
   * @param {CoreSidebarService} _coreSidebarService
   */
  constructor(
    private _userListService: UserListService,
    private _coreSidebarService: CoreSidebarService,
    private _coreConfigService: CoreConfigService
  ) {
    this._unsubscribeAll = new Subject();
    // Initialize default pagination values
    this.page.page = 1;
    this.page.limit = 10;
  }

  // Public Methods
  // -----------------------------------------------------------------------------------------------------

  // Change page limit
  changePageLimit() {
    this.loading = true;
    this.allUserParams = {
      ...this.allUserParams,
      length: this.page.limit,
    };
    // Apply changes to admin list stream
    this._userListService.onNewUserListChange.next(this.allUserParams);
  }

  // Toggle sidebar
  toggleSidebar(name): void {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }

  // Change status filter value
  changeStatusVal() {
    this.loading = true;
    if (!this.selectedStatus || !this.selectedStatus?.value) {
      delete this.allUserParams?.filter;
    } else {
      this.allUserParams = {
        ...this.allUserParams,
        filter: this.selectedStatus?.value == "1" ? "active" : "deactive",
      };
    }
    // Apply changes to admin list stream
    this._userListService.onNewUserListChange.next(this.allUserParams);
  }

  // Delete Admin
  deleteUser(adminId: string) {
    this.loading = true;
    // Apply changes to admin list stream
    this._userListService.onAdminDeleteChange.next(adminId);
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------
  /**
   * On init
   */
  ngOnInit(): void {
    // Listen to Admin list stream
    this._userListService.newUserList$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((result) => {
        this.handleTableDataResponse(result);
      });

    // Listen to Admin delete stream
    this._userListService.adminDelete$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((result) => {
        this.handleDeleteAdminResponse(result);
      });

    // Subscribe config change
    this._coreConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((config) => {
        this.setPage({ offset: 0 });
      });

    // Listen to Search admin stream
    this.searchControl.valueChanges
      .pipe(
        debounceTime(200),
        tap((searchVal) => {
          this.loading = true;
          this.allUserParams = {
            ...this.allUserParams,
            search: searchVal,
          };
          // Apply changes admin list change stream
          this._userListService.onNewUserListChange.next(this.allUserParams);
        })
      )
      .subscribe();
  }

  // Handle Admin list response
  private handleTableDataResponse(result) {
    this.loading = false;
    if (result?.page) {
      this.page = { ...this.page, ...result.page };
      this.rows = result.rows;
    }
  }

  // Handle Admin delete response
  private handleDeleteAdminResponse(result) {
    if (result?.status == 1) {
      this.loading = false;
      // Apply changes admin list change stream
      this._userListService.onNewUserListChange.next(this.allUserParams);
    } else {
      this.error = result?.error?.message || "Something went wrong!";
      this.loading = false;
    }
  }

  // Apply new state for page change
  setPage(pageInfo) {
    this.loading = true;
    this.page.page = pageInfo.offset + 1;
    this.allUserParams = {
      ...this.allUserParams,
      page: this.page.page,
      length: this.page.limit,
    };
    // Apply changes admin list change stream
    this._userListService.onNewUserListChange.next(this.allUserParams);
  }

  // Apply sorting to the admin list
  onSort(event) {
    // event was triggered, start sort sequence
    this.loading = true;
    const sort = event.sorts[0];
    this.allUserParams = {
      ...this.allUserParams,
      sort: sort.prop,
      direction: sort.dir,
    };
    // Apply changes admin list change stream
    this._userListService.onNewUserListChange.next(this.allUserParams);
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
    this._userListService.onAdminDeleteChange.next(null);
  }
}
