import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UserEditService } from "app/services/user-edit.service";
import { UserViewService } from "app/services/user-view.service";
import { UserEditComponent } from "./user-edit/user-edit.component";
import { UserListComponent } from "./user-list/user-list.component";
import { UserViewComponent } from "./user-view/user-view.component";

// User page routes
const routes: Routes = [
  {
    path: "user-list",
    component: UserListComponent,
    data: { animation: "UserListComponent" },
  },
  {
    path: "user-view/:id",
    component: UserViewComponent,
    resolve: {
      data: UserViewService,
    },
    data: { path: "view/:id", animation: "UserViewComponent" },
  },
  {
    path: "user-edit/:id",
    component: UserEditComponent,
    resolve: {
      ues: UserEditService,
    },
    data: { animation: "UserEditComponent" },
  },
  {
    path: "user-view",
    redirectTo: "/dashboard/user/user-view/637e1c27d6f20f0567a995e1", // Redirection
  },
  {
    path: "user-edit",
    redirectTo: "/dashboard/user/user-edit/637e1c27d6f20f0567a995e1", // Redirection
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
