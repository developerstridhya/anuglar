import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "app/auth/guards/auth.guards";
import { AccountSettingsComponent } from "./account-settings.component";

// Account settings Routes
const routes: Routes = [
  {
    path: "",
    component: AccountSettingsComponent,
    canActivate: [AuthGuard],
    data: { animation: "account-settings" },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountSettingsRoutingModule {}
