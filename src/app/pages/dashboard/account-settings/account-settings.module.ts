import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { Ng2FlatpickrModule } from "ng2-flatpickr";
import { SharedModule } from "app/shared/shared.module";
import { AccountSettingsComponent } from "./account-settings.component";
import { AccountSettingsRoutingModule } from "./account-settings-routing.module";

@NgModule({
  declarations: [AccountSettingsComponent],
  imports: [
    CommonModule,
    AccountSettingsRoutingModule,
    NgbModule,
    Ng2FlatpickrModule,
    SharedModule,
  ],
})
export class AccountSettingsModule {}
