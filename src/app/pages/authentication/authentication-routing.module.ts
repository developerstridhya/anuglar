import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthForgotPasswordComponent } from "./auth-forgot-password/auth-forgot-password.component";
import { AuthLoginComponent } from "./auth-login/auth-login.component";
import { AuthRegisterComponent } from "./auth-register/auth-register.component";
import { AuthResetPasswordComponent } from "./auth-reset-password/auth-reset-password.component";

// auth routes
const routes: Routes = [
  {
    path: "login",
    component: AuthLoginComponent,
  },
  {
    path: "register",
    component: AuthRegisterComponent,
  },
  {
    path: "reset-password",
    component: AuthResetPasswordComponent,
  },
  {
    path: "forgot-password",
    component: AuthForgotPasswordComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
