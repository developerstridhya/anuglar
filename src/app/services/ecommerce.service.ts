import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import { EcommerceConstants } from "app/global-data/ecommerce.constants";
import {
  Brands,
  Categories,
  FilterList,
  MultiRange,
  PriceRange,
} from "app/global-data/main-constants";

import { of, BehaviorSubject, Observable, throwError } from "rxjs";
import {
  catchError,
  tap,
  debounceTime,
  switchMap,
  filter,
} from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class EcommerceService implements Resolve<any> {
  // Public
  public productList: Array<any>;
  public filteredProductList: Array<any>;
  public wishlist: Array<any>;
  public cartList: Array<any>;
  public selectedProduct;
  public relatedProducts;

  // Product operations
  public onProductListChange: BehaviorSubject<any>;
  public onRelatedProductsChange: BehaviorSubject<any>;
  public onSelectedProductChange: BehaviorSubject<any>;
  public onProductEditChange: BehaviorSubject<any>;
  public onProductEditLoader: BehaviorSubject<any>;
  public onNewProductChange: BehaviorSubject<any>;
  public newproduct$: Observable<any>;

  // Filter Values & Operations
  public onInitialFilterChange: BehaviorSubject<any>;
  public productFilter$: Observable<any>;
  public initialFilter = EcommerceConstants.initialFilter;
  public initialMultiRange = EcommerceConstants.initialMultiRange;
  public initialPriceRange = EcommerceConstants.initialPriceRange;
  public initialCategories = EcommerceConstants.initialCategories;
  public initialBrands = EcommerceConstants.initialBrands;
  public initialRating = EcommerceConstants.initialRating;

  // Private
  private idHandel;
  private sortRef = (key) => (a, b) => {
    const fieldA = a[key];
    const fieldB = b[key];

    let comparison = 0;
    if (fieldA > fieldB) {
      comparison = 1;
    } else if (fieldA < fieldB) {
      comparison = -1;
    }
    return comparison;
  };

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _router: Router) {
    this.onProductEditLoader = new BehaviorSubject(false);
    this.onProductListChange = new BehaviorSubject({});
    this.onRelatedProductsChange = new BehaviorSubject({});
    this.onSelectedProductChange = new BehaviorSubject({});
    this.initializeFilter(); // Initialze Empty filter
  }

  // Initialize the Filter stream
  private initializeFilter() {
    this.onInitialFilterChange = new BehaviorSubject(this.initialFilter);
    // Assign Filter change stream
    this.productFilter$ = this.onInitialFilterChange.asObservable().pipe(
      debounceTime(100),
      tap((item) => this.triggerFilterProducts(item))
    );
  }

  initializeProductEdit() {
    this.onProductEditChange = new BehaviorSubject(null);
    // Assign product edit change stream
    return this.onProductEditChange.asObservable().pipe(
      // Prevent the repeated valid API calls
      debounceTime(200),
      filter((item) => item != null),
      switchMap((product) => this.updateProductEdit(product))
    );
  }

  initializeProductCreate() {
    this.onNewProductChange = new BehaviorSubject(null);
    // Assign New product change stream
    return this.onNewProductChange.asObservable().pipe(
      // Prevent the repeated valid API calls
      debounceTime(200),
      filter((item) => item != null),
      switchMap((product) => this.registerProductProcess(product))
    );
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    this.idHandel = route.params.id;
    const apiList = [this.getSelectedProduct(), this.getProducts()];
    return new Promise<void>((resolve, reject) => {
      Promise.all(apiList).then(() => {
        resolve();
      }, reject);
    });
  }

  // Get Ecommerce products list API
  getProducts(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .get("api/ecommerce-products")
        .subscribe((response: any) => {
          this.productList = response;
          this.sortProduct("featured"); // Default shorting
          resolve(this.productList);
        }, reject);
    });
  }

  // Get Ecommerce selected products list API
  getSelectedProduct(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .get("api/ecommerce-products?id=" + this.idHandel)
        .subscribe((response: any) => {
          this.selectedProduct = response;
          this.onSelectedProductChange.next(this.selectedProduct);
          resolve(this.selectedProduct);
        }, reject);
    });
  }

  // Get Ecommerce related products list API
  getRelatedProducts(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .get("api/ecommerce-relatedProducts")
        .subscribe((response: any) => {
          this.relatedProducts = response;
          this.onRelatedProductsChange.next(this.relatedProducts);
          resolve(this.relatedProducts);
        }, reject);
    });
  }

  // Edit Ecommerce product API
  private editProduct(product: any) {
    return this._httpClient.put(
      "api/ecommerce-products/" + product.id,
      product
    );
  }

  // Register Ecommerce product API
  private registerNewProduct(product: any) {
    return this._httpClient.post("api/ecommerce-products/", product);
  }

  //#region register products functions
  registerProductProcess(newProduct) {
    return this.registerNewProduct(newProduct).pipe(
      // Handle Error
      catchError((err) => {
        return of(err);
      }),
      // Apply Side effects
      tap((response: any) => {
        this.updateNewProduct(response);
      })
    );
  }

  // Update new product on stream
  private updateNewProduct(newItem) {
    this.productList = [newItem, ...this.productList];
    // Apply new product addition to product list stream
    this.onProductListChange.next(this.productList);
  }
  //#endregion

  //#region Toggle product availibity
  private updateProductEdit(editedProduct) {
    return this.editProduct(editedProduct).pipe(
      // Handle error
      catchError((err) => {
        return of(err);
      }),
      // Apply Side effects
      tap((response: any) => {
        this.updateProductValue(response);
      })
    );
  }

  // Filter & Update product value conditionally
  private updateProductValue(editedResult) {
    if (this._router.url.includes("edit")) {
      // Redirect if ecommerce edit page detected
      this._router.navigate([
        "/dashboard/e-commerce/details/" + editedResult?.id,
      ]);
    } else {
      let prodIdx;
      prodIdx = this.productList.findIndex(
        (item) => item.id == editedResult.id
      );
      if (prodIdx != -1) {
        this.productList[prodIdx] = editedResult;
        // Apply changes to product list stream
        if (!this.filteredProductList)
          this.onProductListChange.next(this.productList);
        this.selectedProduct = [editedResult];
        // Apply changes to selected product list stream
        this.onSelectedProductChange.next(this.selectedProduct);
      }
      if (this.filteredProductList) {
        prodIdx = this.filteredProductList.findIndex(
          (item) => item.id == editedResult.id
        );
        if (prodIdx != -1) {
          this.filteredProductList[prodIdx] = editedResult;
          // Apply changes to product list stream
          this.onProductListChange.next(this.filteredProductList);
        }
      }
    }
  }
  //#endregion

  // Sort product by key
  sortProduct(sortBy) {
    let sortDesc = false;

    const sortByKey = (() => {
      if (sortBy === "price-desc") {
        sortDesc = true;
        return "price";
      }
      if (sortBy === "price-asc") {
        return "price";
      }
      sortDesc = true;
      return "id";
    })();

    const sortedData = this.productList.sort(this.sortRef(sortByKey));
    if (sortDesc) sortedData.reverse();
    this.productList = sortedData;
    // Apply changes to product list stream
    this.onProductListChange.next(this.productList);
  }

  // Sort products by label
  getSortLabel(sortBy) {
    switch (sortBy) {
      case "featured": {
        return "Featured";
      }
      case "price-asc": {
        return "Lowest";
      }
      case "price-desc": {
        return "Highest";
      }
      default: {
        return "Featured";
      }
    }
  }

  //#region Sidebar Filter
  private triggerFilterProducts(filterList: FilterList) {
    setTimeout(() => {
      // Update Actual filter by resolving all the filters values
      this.filteredProductList = [...this.productList];
      this.filteredProductList = this.multiRangeFilter(filterList.multiRange);
      this.filteredProductList = this.priceRangeFilter(
        filterList.priceRangeFilter
      );
      this.filteredProductList = this.categoriesFilter(filterList.categories);
      this.filteredProductList = this.brandsFilter(filterList.brands);
      this.filteredProductList = this.ratingFilter(filterList.ratings);
      // Apply changes to product list stream
      this.onProductListChange.next(this.filteredProductList);
    }, 0);
  }

  //#region  Multirange filter
  private multiRangeFilter(multiRange: MultiRange) {
    const multiRangeOp = this.getOperationDetails(multiRange);
    if (multiRangeOp == null) return this.filteredProductList;
    return this.filteredProductList.filter(
      (item) => item.price >= multiRangeOp.min && item.price <= multiRangeOp.max
    );
  }

  // Decide on operatin details to perform upon filter
  private getOperationDetails(multiRange: MultiRange) {
    if (multiRange == MultiRange.ALL) return null;
    else if (multiRange == MultiRange.FROM_10_TO_100) {
      return { min: 10, max: 100 };
    } else if (multiRange == MultiRange.FROM_100_TO_500)
      return { min: 100, max: 500 };
    else if (multiRange == MultiRange.LESS_EQ_10) return { min: 0, max: 10 };
    else if (multiRange == MultiRange.GREAT_EQ_500)
      return { min: 500, max: 9999999 };
  }
  //#endregion

  //#region Price Range Filter
  private priceRangeFilter(priceRange: PriceRange) {
    if (priceRange.min === 1 && priceRange.max === 100)
      return this.filteredProductList;
    return this.filteredProductList.filter(
      (item) => item.price >= priceRange.min && item.price <= priceRange.max
    );
  }
  //#endregion

  //#region Categories Filter
  private categoriesFilter(category: Categories) {
    if (category === Categories.APPLIANCES) return this.filteredProductList;
    return this.filteredProductList.filter(
      (item) => item.categoery.toLowerCase() == category
    );
  }
  //#endregion

  // #region Categories Filter
  private brandsFilter(brands: Brands[]) {
    if (brands.length === 0) return this.filteredProductList;
    return this.filteredProductList.filter((item) =>
      brands.includes(item.brand.toLowerCase())
    );
  }
  // #endregion

  // #region rating filter
  private ratingFilter(ratings: number) {
    if (ratings === 0) return this.filteredProductList;
    return this.filteredProductList.filter((item) =>
      ratings == 4 ? item.rating >= ratings : item.rating === ratings
    );
  }
  // #endregion

  //#endregion
}
