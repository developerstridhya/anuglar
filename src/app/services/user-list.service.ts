import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";

import { of, BehaviorSubject, Observable, throwError, EMPTY } from "rxjs";
import {
  catchError,
  map,
  debounceTime,
  filter,
  switchMap,
} from "rxjs/operators";

@Injectable({ providedIn: "root" })
export class UserListService {
  public onNewUserListChange: BehaviorSubject<any>;
  public newUserList$: Observable<any>;

  public onAdminDeleteChange: BehaviorSubject<any>;
  public adminDelete$: Observable<any>;

  public onAdminCreateChange: BehaviorSubject<any>;
  public adminCreate$: Observable<any>;

  constructor(private _httpClient: HttpClient) {
    this.initializeUserList(); // Initialize User list stream
    this.initializeAdminDelete(); // Initialize Admin Delete stream
    this.initializeAdminCreate(); // Initialize Admin Create stream
  }

  // Initialize Admin Create stream
  private initializeUserList() {
    this.onNewUserListChange = new BehaviorSubject(null);
    this.newUserList$ = this.onNewUserListChange.asObservable().pipe(
      // Prevent the repeated valid API calls
      debounceTime(200),
      filter((item) => item != null),
      switchMap((product) =>
        this.getDataTableRows(product).pipe(
          // Handle error
          catchError((err) => {
            return of(err);
          }),
          // Filter and Map data as per paginated table
          filter((item: any) => item.status == 1),
          map((pagedData: any) => {
            return this.mapTableData(pagedData);
          })
        )
      )
    );
  }

  // Initialize Admin Delete stream
  private initializeAdminDelete() {
    this.onAdminDeleteChange = new BehaviorSubject(null);
    this.adminDelete$ = this.onAdminDeleteChange.asObservable().pipe(
      // Prevent the repeated valid API calls
      debounceTime(200),
      filter((item) => item != null),
      switchMap((adminId) =>
        this.deleteAdmin(adminId).pipe(
          // Handle error
          catchError((err) => {
            return of(err);
          })
        )
      )
    );
  }

  // Initialize Admin Delete stream
  private initializeAdminCreate() {
    this.onAdminCreateChange = new BehaviorSubject(null);
    this.adminCreate$ = this.onAdminCreateChange.asObservable().pipe(
      // Prevent the repeated valid API calls
      debounceTime(200),
      filter((item) => item != null),
      switchMap((adminBody) =>
        this.registerNewUser(adminBody).pipe(
          // Handle error
          catchError((err) => {
            return of(err);
          })
        )
      )
    );
  }

  // Get Admin list API
  private getDataTableRows(userParams: any) {
    return this._httpClient.get(`${environment.apiUrl}/admin/admin-list`, {
      params: userParams,
    });
  }

  // Register Admin list API
  private registerNewUser(userBody: any) {
    return this._httpClient.post(
      `${environment.apiUrl}/admin/create-admin`,
      userBody
    );
  }

  // Delete Admin list API
  private deleteAdmin(adminId: string) {
    return this._httpClient.put(
      `${environment.apiUrl}/admin/delete-admin/${adminId}`,
      {}
    );
  }

  // Map Pagination data as per limit & Offset
  private mapTableData(pagedData) {
    const page = {
      totalPages: pagedData?.data?.adminUsersCount || 1,
      totalElements: pagedData?.data?.adminUsersCount || 1,
    };
    const rows = pagedData?.data?.adminList || [];
    return { rows, page };
  }
}
