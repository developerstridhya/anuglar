import { Brands, Categories, MultiRange } from "./main-constants";

export class EcommerceConstants {
  static initialFilter = {
    categories: Categories.APPLIANCES,
    multiRange: MultiRange.ALL,
    priceRangeFilter: { min: 1, max: 100 },
    ratings: 0,
    brands: [],
  };

  static initialMultiRange = [
    { label: "All", value: MultiRange.ALL, checked: false },
    { label: "<=$10", value: MultiRange.LESS_EQ_10, checked: false },
    { label: "$10 - $100", value: MultiRange.FROM_10_TO_100, checked: false },
    { label: "$100 - $500", value: MultiRange.FROM_100_TO_500, checked: false },
    { label: ">= $500", value: MultiRange.GREAT_EQ_500, checked: false },
  ];

  static initialPriceRange = [1, 100];

  static initialCategories = [
    { label: "Appliances", value: Categories.APPLIANCES, checked: false },
    { label: "Audio", value: Categories.AUDIO, checked: false },
    {
      label: "Cameras & Camcorders",
      value: Categories.CAMERAS_CAMCORDERS,
      checked: false,
    },
    {
      label: "Car Electronics & GPS",
      value: Categories.CAR_ELECTRONICS_GPS,
      checked: false,
    },
    { label: "Cell Phones", value: Categories.CELL_PHONES, checked: false },
    {
      label: "Computers & Tablets",
      value: Categories.COMPUTERS_TABLETS,
      checked: false,
    },
    {
      label: "Health, Fitness & Beauty",
      value: Categories.HEALTH_FITNESS_BEAUTY,
      checked: false,
    },
    {
      label: "Office & School Supplies",
      value: Categories.OFFICE_SCHOOL_SUPPLIES,
      checked: false,
    },
    {
      label: "TV & Home Theater",
      value: Categories.TV_HOME_THEATER,
      checked: false,
    },
    { label: "Video Games", value: Categories.VIDEO_GAMES, checked: false },
  ];

  static initialBrands = [
    { key: Brands.APPLE, checked: false, counts: 746 },
    { key: Brands.ONEODIO, checked: false, counts: 677 },
    { key: Brands.SHARP, checked: false, counts: 625 },
    { key: Brands.GOOGLE, checked: false, counts: 567 },
    { key: Brands.PHILIPS, checked: false, counts: 514 },
    { key: Brands.LOGITECH, checked: false, counts: 497 },
    { key: Brands.NIKE, checked: false, counts: 456 },
    { key: Brands.BUGANI, checked: false, counts: 411 },
    { key: Brands.SONY, checked: false, counts: 402 },
    { key: Brands.TAS, checked: false, counts: 378 },
    { key: Brands.ADIDAS, checked: false, counts: 321 },
  ];

  static initialRating = [
    { rating: 4, counts: 160 },
    { rating: 3, counts: 176 },
    { rating: 2, counts: 291 },
    { rating: 1, counts: 190 },
  ];
}
